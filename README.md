### 官方网站:http://dzzoffice.com
### 官方演示地址:http://demo.dzzoffice.com
### dzzoffice小胡版：https://gitee.com/xh2002/dzzoffice
### 【金山文档】 Dzzoffice笔记 https://kdocs.cn/l/ccpEhPqsY3na
### DzzOffice小胡版插件安装和升级说明：

#### 重点：小胡版插件基于dzzoffice小胡版编辑，其他版本不确定兼容性。

1. 下载解压后，将news文件夹放在网站dzz\下(如果该目录下已有相同文件夹请先删除在放)

2. 升级的用户将update.php、install.sql放在网站根目录下，然后访问：域名/update.php按提示操作。为了数据安全，升级后最好删除update.php、install.sql。

3. 安装的用户，在应用市场右下角选择导入，选择导入文件，导入文件是文件夹下的安装文档.txt，然后选择提交。